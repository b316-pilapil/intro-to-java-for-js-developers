package com.zuitt.example;
import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Howe old are you?: ");

        double age = new Double(scanner.nextDouble());
        System.out.println("You are "+age+ " y.o.");
    }
}
