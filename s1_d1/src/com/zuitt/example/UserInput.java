package com.zuitt.example;
import java.util.Scanner;

public class UserInput {

    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in); //create scanner
        System.out.println("User a username");

        String username = myObj.nextLine(); // read input

        System.out.println("Username is "+username);
    }
}
