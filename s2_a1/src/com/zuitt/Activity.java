package com.zuitt;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");
        int year = scanner.nextInt();

        System.out.println(year+" is "+(year%4==0 && (year%100!=0 || year%400==0)?"":"NOT ")+ "a leap year");
        
    }
}
