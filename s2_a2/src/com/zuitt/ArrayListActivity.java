package com.zuitt;

import java.util.ArrayList;

public class ArrayListActivity {
    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<String>();
        names.add("John");
        names.add("Jane");
        names.add("Chloe");
        names.add("Zoey");
        System.out.println("My friends are: "+names);
    }
}
