package com.zuitt;

public class PrimeNumber {
    public static void main(String[] args) {   
        int[] primes = {2,3,5,7,11};
        System.out.println("The first prime number is: "+primes[0]);
    }
}
