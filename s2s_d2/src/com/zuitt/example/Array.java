package com.zuitt.example;
import java.util.ArrayList;
import java.util.HashMap;

public class Array {
    public static void main(String[] args) {
        int[] intArray = new int[5];
        int[] intArray2 = {100, 200, 300, 400, 500};
        String[][] classroom = new String[3][3];
        //row major
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porhos";
        classroom[0][2] = "Aramis";

        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";

        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        ArrayList<String> students = new ArrayList<String>();
        students.add("Jonh");
        students.add("Paul");
        students.get(0);
        students.set(1, "George"); //overwrite
        students.remove(1);
        students.clear();
        System.out.println(students.size());

        //hashmap
        HashMap<String, String> job_position = new HashMap<>();
        job_position.put("Brandon", "Student"); //insert
        job_position.put("Alice", "Dreamer");
        job_position.get("Alice");
        System.out.println(job_position.keySet());
        job_position.remove("Brandon");
        System.out.println(job_position);
    }
}
