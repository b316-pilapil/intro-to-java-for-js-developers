package com.zuitt.example;

public class ControlStructures {
    public static void main(String[] args) {
        int n1 = 10, n2=20;
        if(n1>5){
            System.out.println("n1 > 5");
        }

        if(n2>100){
            System.out.println("n2 >100");
        }else{
            System.out.println("n2 < 100");
        }

        int x=15, y=1;
//        if(y>1 || x/y==0){
//            System.out.println("res = "+x/y);
//        }else{
//            System.out.println("error");
//        }

        int directionValue = 4;
        switch(directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
        }
    }
}
