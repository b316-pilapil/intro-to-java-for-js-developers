package com.zuitt;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a number to compute its factorial: ");
        int num=0;
        try {
            num = scanner.nextInt();
            int ctr=1, ans=1;
            if(num>-1){
                while(ctr<=num){
                    ans*=ctr++;
                }
                System.out.println("The factorial of "+num+" is "+ans+ " (while loop)");

                for (ctr = 1, ans=1; ctr <= num; ans*=ctr++) {}
                System.out.println("The factorial of "+num+" is "+ans+ " (for loop)");
            }else{
                System.out.println("Cannot calculate for negative numbers");
            }
        } catch (Exception e) {
            System.out.println("Input is not a number.");
            // e.printStackTrace();
        }
    }
}

