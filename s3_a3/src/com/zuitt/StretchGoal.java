package com.zuitt;

import java.util.Scanner;

public class StretchGoal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a number of stars: ");
        int num=0;
        try {
            num = scanner.nextInt();
            if(num>0){
                for(int i=1;i<=num;i++){
                    for(int j=1;j<=i;j++, System.out.print("* ")){}
                    System.out.print("\n");
                }
            }else{
                System.out.println("Input must be a positive number");
            }
        }catch(Exception e){
            System.out.println("Input is not a number.");
        }
    }
}
