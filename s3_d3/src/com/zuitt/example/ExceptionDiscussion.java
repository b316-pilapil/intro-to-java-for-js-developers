package com.zuitt.example;

import java.util.Scanner;

public class ExceptionDiscussion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a number: ");
        int num=0;
        try {
            num = scanner.nextInt();
        } catch (Exception e) {
            System.out.println("input not int");
            e.printStackTrace();
        }
        System.out.println("num: "+num);
        
    }
}
