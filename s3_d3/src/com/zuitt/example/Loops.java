package com.zuitt.example;

public class Loops {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println("cnt: "+i);
        }
        int[] intArray = {100,200,300,400,500};
        for (int i = 0; i < intArray.length; i++) {
            System.out.println(i);
        }
        String[] nameArray =  {"john", "paul", "george", "ringo"};
        for(String name: nameArray){
            System.out.println(name);
        }
        String[][] classroom = new String[3][3];
        //row major
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porhos";
        classroom[0][2] = "Aramis";

        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";

        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for (int i = 0; i < classroom.length; i++) {
            for (int j = 0; j < classroom[i].length; j++) {
                System.out.println(classroom[i][j]);
            }
        }

        int x = 0, y=10;
        while(x<10){
            System.out.println("num:" +x++);
        }
        do{
            System.out.println("y: "+y);
        }while(y>10);
    }
}
