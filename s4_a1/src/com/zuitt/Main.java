package com.zuitt;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        User u = new User();
        Course c = new Course();

        u.setName("Terrence Gaffud");
        u.setAge(25);
        u.setEmail("tgaff@mail.com");
        u.setAddress("Quezon City");

        c.setName("MACQ004");
        c.setDescription("An introduction to Java for career shifters");
        c.setFee(5000.00);
        c.setSeats(15);
        c.setStartDate(new Date());
        c.setEndDate(new Date(new Date().getTime() + 5*86400000));
        c.setInstructor(u);

        System.out.println("Hi! I'm "+u.getName()+". I'm "+u.getAge()+" .You can reach me via my email: "+u.getEmail()+". When I'm off work, I can be found at my house in "+u.getAddress());
        System.out.println("Welcome to the course "+c.getName()+". This course can be described as"+c.getDescription()+". Your instructor for this course is Sir "+c.getInstructor().getName()+". Enjoy!");
    }
}
