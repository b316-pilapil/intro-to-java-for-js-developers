package com.zuitt.example;

public class Car {
    private String make;
    private String brand;
    private int price;
    private Driver carDriver;

    public Car() {
        this.carDriver = new Driver();
    }
    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

//    @Override
//    public String toString(){
//        return "Car with:: Brand: "+this.brand+", Make: "+this.make+", price: "+this.price;
//    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    public String getCarDriverName(){
        return this.carDriver.getName();
    }

    public void start(){
        System.out.println("Vroom vroom!");
    }
}
