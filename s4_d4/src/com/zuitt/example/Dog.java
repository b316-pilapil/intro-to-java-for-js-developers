package com.zuitt.example;

public class Dog extends Animal{
    private String dogBreed;

    public Dog() {
        super();
        this.dogBreed = "Chihuahua";
    }
    public Dog(String name, String color, String dogBreed) {
        super(name, color);
        this.dogBreed = dogBreed;
    }

    public String getDogBreed() {
        return dogBreed;
    }

    public void setDogBreed(String dogBreed) {
        this.dogBreed = dogBreed;
    }

    public void greet(){
        super.call();
        System.out.println("Bark!");
    }
}
