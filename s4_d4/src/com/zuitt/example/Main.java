package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car car1=new Car();
//        car1.setBrand("Bugatti");
//        car1.setPrice(200000);
//        System.out.println(car1);

        Car car2 = new Car();
//        car2.make="Wigo";
//        car2.brand="Toyota";
//        car2.price=40000;
//        System.out.println(car2);
//
        Car car3 = new Car();
//        car3.make="Racecar";
//        car3.brand="Ferrari";
//        car3.price=2000000;
//        System.out.println(car3);
        Driver driver1= new Driver("Alejandro", 25);
        car1.setCarDriver(driver1);

        car1.start();
        car2.start();
        car3.start();

        System.out.println(car1.getMake());
        System.out.println(car2.getMake());
        car1.setMake("Veyron");
        car2.setMake("Innova");
        System.out.println(car1.getMake());
        System.out.println(car2.getMake());

        System.out.println(car1.getCarDriver().getName());
        Driver newDriver = new Driver("Antonio", 21);
        car1.setCarDriver(newDriver);
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        Animal a = new Animal("Clifford", "red");
        a.call();

        Dog d = new Dog();
        System.out.println(d.getName());
        d.call();
        d.setName("Hachiko");
        System.out.println(d.getName());
        d.call();
        d.setColor("Brown");
        System.out.println(d.getColor());

        Dog d2 = new Dog("Mike", "Dark brown", "Corgi");
        d2.greet();
    }
}
