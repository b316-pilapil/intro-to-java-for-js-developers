package com.zuitt;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Phonebook ph = new Phonebook();
        Contact c1 = new Contact("John Doe", "+639228547963", "my home in Quezon City"),
                c2 = new Contact("Jane Doe", "+639162148573", "my home in Caloocan City");

        display(ph);
        System.out.println();
        ph.setContacts(c1);
        ph.setContacts(c2);
        display(ph);

        System.out.println("\nStretch goal");
        User u = new User();
        Course c = new Course();

        u.setName("Terrence Gaffud");
        u.setAge(25);
        u.setEmail("tgaff@mail.com");
        u.setAddress("Quezon City");

        c.setName("MACQ004");
        c.setDescription("An introduction to Java for career shifters");
        c.setFee(5000.00);
        c.setSeats(15);
        c.setStartDate(new Date());
        c.setEndDate(new Date(new Date().getTime() + 5*86400000));
        c.setInstructor(u);

        System.out.println("Hi! I'm "+u.getName()+". I'm "+u.getAge()+" .You can reach me via my email: "+u.getEmail()+". When I'm off work, I can be found at my house in "+u.getAddress());
        System.out.println("Welcome to the course "+c.getName()+". This course can be described as"+c.getDescription()+". Your instructor for this course is Sir "+c.getInstructor().getName()+". Enjoy!");

    }

    public static void display(Phonebook ph){
        if(ph.getContacts().isEmpty()){
            System.out.println("Phonebook is empty");
        }else{
            for (Contact c: ph.getContacts()){
                System.out.println("Name: "+c.getName());
                System.out.println("Contact number: "+c.getContactNumber());
                System.out.println("Address: "+c.getAddress());
                System.out.println("===================================================================");
            }
        }
    }
}
