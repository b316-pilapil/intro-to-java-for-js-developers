package com.zuitt;

import java.util.ArrayList;

public class Phonebook {
    ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook() {
    }

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Contact contacts) {
        this.contacts.add(contacts);
    }
}
