package com.zuitt.example;

public interface Action {
    public void sleep();
    public void run();
    public void eat();
    public void drink();
}
