package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("hw");
        Person p = new Person();
        p.run();
        p.sleep();
        p.eat();
        p.drink();

        StaticPoly staticPoly = new StaticPoly();
        System.out.println(staticPoly.addition(1,5));
        System.out.println(staticPoly.addition(1,5, 10));
        System.out.println(staticPoly.addition(15.5,15.6));

        Parent pa = new Parent("John", 35);
        pa.greet();
        pa.greet("John", "am");
        pa.speak();
        Child ch = new Child();
        ch.speak();
    }
}
